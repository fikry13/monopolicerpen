﻿using UnityEngine;
using System.Collections;

namespace Monopoly
{
    public class TileManager : MonoBehaviour
    {
        public Tile[] tile;

        public GameObject player;
        public int currentPlayerPosition;

        // Use this for initialization
        void Start()
        {
            Init();
        }

        // Update is called once per frame
        void Update()
        {
            for (int i = 0; i < tile.Length; i++)
            {
                Vector2 playerPosition = player.transform.position;
                if ((playerPosition.x > tile[i].first.x && playerPosition.y > tile[i].first.y) && (playerPosition.x < tile[i].second.x && playerPosition.y < tile[i].second.y))
                {
                    tile[i].isPlayerHere = true;
                    currentPlayerPosition = i;
                }
                else
                    tile[i].isPlayerHere = false;
            }
        }

        void Init()
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    }
}
