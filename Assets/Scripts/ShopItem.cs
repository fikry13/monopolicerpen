﻿using UnityEngine;
using System.Collections;

namespace Monopoly
{
    [RequireComponent(typeof(ButtonControl))]
    public class ShopItem : MonoBehaviour
    {
        ButtonControl buttonControl;

        [SerializeField]
        GameObject item;

        public int id;
        public bool isUnlock;
        public Sprite lockIcon;
        public Sprite unlockIcon;

        void Start()
        {
            buttonControl = GetComponent<ButtonControl>();
        }

        void Update()
        {
            if (!isUnlock)
            {
                buttonControl.buttonIcon = lockIcon;
                buttonControl.SetButtonIcon();
            }
            else
            {
                buttonControl.buttonIcon = unlockIcon;
                buttonControl.SetButtonIcon();
            }
        }

        [Signal]
        void ItemTest()
        {
            if(isUnlock)
            {
                GameObject clone = Instantiate(item, transform.localPosition, Quaternion.identity) as GameObject;
                Debug.Log("Item Clicked");
                clone.transform.localPosition = transform.position;
            }
            else
            {

            }
        }

    }
}
