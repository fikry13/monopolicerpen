﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TweenControl : MonoBehaviour {

    public SingleTween[] tweens;
    public bool autoPlay = true;
    public float delay = 0;
    public bool autoKill = true;

    public delegate void Complete();
    public Complete OnComplete;

    float longestTweenDuration;
    Tweener longestTween;

	// Use this for initialization
	void Start () {
        if (autoPlay) StartTween();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartTween()
    {
        Tweener tween = transform.DOMove(new Vector3(0,0,0),0);        

        for (int i = 0; i < tweens.Length; i++)
        {
            float duration = tweens[i].duration;
            TweenType tweenType = tweens[i].tweenType;
            Vector3 value = new Vector3(tweens[i].tweenValue.x, tweens[i].tweenValue.y, transform.localPosition.z);
            if (tweens[i].isBounce)
            {
                switch (tweens[i].tweenType)
                {
                    case TweenType.Move:
                        tween = transform.DOPunchPosition(value, duration);
                        break;
                    case TweenType.Fade:
                        tween = gameObject.renderer.material.DOFade(value.x, duration);
                        break;
                    case TweenType.Rotate:
                        tween = transform.DOPunchRotation(value, duration);
                        break;
                    case TweenType.Scale:
                        tween = transform.DOPunchScale(value, duration);
                        break;
                }
            }
            else
            {
                switch (tweens[i].tweenType)
                {
                    case TweenType.Move:
                        tween = transform.DOMove(value, duration);
                        break;
                    case TweenType.Fade:
                        tween = gameObject.renderer.material.DOFade(value.x, duration);
                        break;
                    case TweenType.Rotate:
                        tween = transform.DORotate(value, duration);
                        break;
                    case TweenType.Scale:
                        tween = transform.DOScale(value, duration);
                        break;
                }
            }

            if (tweens[i].isFrom) tween.From();
            tween.SetAutoKill(autoKill);
            tween.SetDelay(delay);
            tween.SetEase(tweens[i].ease);
            longestTween = tween;
            if (longestTweenDuration <= tweens[i].duration)
            {
                longestTweenDuration = tweens[i].duration;
                longestTween = tween;
                if(OnComplete != null)
                {
                    tween.OnComplete(() => OnComplete());
                }

            }
        }
    }
}

[System.Serializable]
public class SingleTween
{
    public TweenType tweenType;
    public Vector3 tweenValue;
    public float duration;
    public bool isBounce, isFrom;
    public Ease ease;
}

public enum TweenType
{
    Move,
    Rotate,
    Scale,
    Fade
}