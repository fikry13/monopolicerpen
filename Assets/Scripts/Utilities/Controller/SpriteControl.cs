﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteControl : MonoBehaviour {
    
    /// <summary>
    /// Animation FPS
    /// </summary>
    public float framePerSecond = 5;
    /// <summary>
    /// Current Frame index that being rendered on SpriteRendere
    /// </summary>
    public int currentFrame = 0;
    /// <summary>
    /// Default frame index to repeat, if the animation was looping or repeating
    /// </summary>
    public int repeatToFrame = 0;
    /// <summary>
    /// How many times the animation will repeat.
    /// </summary>
    public int repeatCount = 0;
    /// <summary>
    /// Is the animation playing or not
    /// </summary>
    public bool isPlaying;
    /// <summary>
    /// Play the naimation after it finish
    /// </summary>
    public bool isLoop;
    /// <summary>
    /// Play animation forward and backward
    /// </summary>
    public bool isPingPong;
    /// <summary>
    /// List contains frame that used in animation
    /// </summary>
    public List<Sprite> frame;

    private bool isForward = true;
    private float timePerFrame;

	// Use this for initialization
	void Start () {
        timePerFrame = 1f / framePerSecond;
        GetComponent<SpriteRenderer>().sprite = frame[0];
	}
	
    /// <summary>
    /// Set currentFrame on the animation
    /// </summary>
    /// <param name="frameNumber"></param>
    public void SetFrame(int frameNumber)
    {
        currentFrame = frameNumber;
        GetComponent<SpriteRenderer>().sprite = frame[frameNumber];
    }
    
    /// <summary>
    /// Set current frame to the next frame
    /// </summary>
    public void NextFrame()
    {
        if (currentFrame == frame.Count - 1)
        {
            currentFrame = 0;
        }
        else
            currentFrame++;

        GetComponent<SpriteRenderer>().sprite = frame[currentFrame];
    }

    /// <summary>
    /// Set current frame to the previous frame
    /// </summary>
    public void PrevFrame()
    {
        if (currentFrame == 0)
        {
            currentFrame = frame.Count - 1;
        }
        else
            currentFrame--;

        GetComponent<SpriteRenderer>().sprite = frame[currentFrame];
    }

	// Update is called once per frame
	void Update ()
    {
        if (repeatCount > 0)
            isLoop = false;

        if(isPlaying)
        {
            timePerFrame = timePerFrame - Time.deltaTime;
            if(timePerFrame <= 0)
            {
                timePerFrame = 1f / framePerSecond;

                if(!isPingPong)
                {
                    if (currentFrame == frame.Count - 1)
                    {
                        if(repeatCount != 0) 
                            repeatCount--;
                        if (!isLoop && (repeatCount == 0))
                            isPlaying = false;
                        if(isPlaying)
                            currentFrame = repeatToFrame;
                    }
                    else
                        currentFrame++;
                }
                else
                {
                    if (currentFrame == frame.Count - 1)
                    {
                        isForward = false;
                    }
                    else if (currentFrame == repeatToFrame)
                    {
                        if (!isForward && repeatCount != 0)
                            repeatCount--;
                        if (!isForward && (!isLoop && (repeatCount == 0)))
                            isPlaying = false;
                        isForward = true;
                    }
                    if (isPlaying)
                    {
                        if (isForward)
                            currentFrame++;
                        else
                            currentFrame--;
                    }                    
                }
            }

            GetComponent<SpriteRenderer>().sprite = frame[currentFrame];
        }
	}
}
