﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class AnimationControl : MonoBehaviour 
{
    /// <summary>
    /// Current Selected Animation
    /// </summary>
    public int currentAnimation;
    /// <summary>
    /// Default Animation
    /// </summary>
    public int defaultAnimation;
    /// <summary>
    /// Check if the animation is playing or not
    /// </summary>
    public bool isAnimationPlaying;
    /// <summary>
    /// List contain animation sprite
    /// </summary>
    public List<AnimationSpriteCollection> sprites;

    private AnimationSpriteCollection currentSprite;
    private bool isForward = true;
    private float timePerFrame;

    /// <summary>
    /// Set current animation
    /// </summary>
    /// <param name="index">Index of the sprite container</param>
    public void SetAnimation(int index)
    {
        currentAnimation = index;
    }

    /// <summary>
    /// Set current animation
    /// </summary>
    /// <param name="name">Name of the sprite collection</param>
    public void SetAnimation(string name)
    {
        for (int i = 0 ; i < sprites.Count ; i++)
        {
            if (name.Equals(sprites[i].name))
                currentAnimation = i;
        }
    }

    // Use this for initialization
    void Start()
    {
        currentAnimation = defaultAnimation;
    }
	
	// Update is called once per frame
	void Update () 
    {
        currentSprite = sprites[currentAnimation];

	    if(isAnimationPlaying)
        {
            if (currentSprite.repeatCount > 0)
                currentSprite.isLoop = false;

            if (currentSprite.isPlaying)
            {
                timePerFrame = timePerFrame - Time.deltaTime;
                if (timePerFrame <= 0)
                {
                    timePerFrame = 1f / currentSprite.framePerSecond;

                    if (!currentSprite.isPingPong)
                    {
                        if (currentSprite.currentFrame == currentSprite.frame.Count - 1)
                        {
                            if (currentSprite.repeatCount != 0)
                                currentSprite.repeatCount--;
                            if (!currentSprite.isLoop && (currentSprite.repeatCount == 0))
                                currentSprite.isPlaying = false;
                            if (currentSprite.isPlaying)
                                currentSprite.currentFrame = currentSprite.repeatToFrame;
                        }
                        else
                            currentSprite.currentFrame++;
                    }
                    else
                    {
                        if (currentSprite.currentFrame == currentSprite.frame.Count - 1)
                        {
                            isForward = false;
                        }
                        else if (currentSprite.currentFrame == currentSprite.repeatToFrame)
                        {
                            if (!isForward && currentSprite.repeatCount != 0)
                                currentSprite.repeatCount--;
                            if (!isForward && (!currentSprite.isLoop && (currentSprite.repeatCount == 0)))
                                currentSprite.isPlaying = false;
                            isForward = true;
                        }
                        if (currentSprite.isPlaying)
                        {
                            if (isForward)
                                currentSprite.currentFrame++;
                            else
                                currentSprite.currentFrame--;
                        }
                    }
                }

                GetComponent<SpriteRenderer>().sprite = currentSprite.frame[currentSprite.currentFrame];
            }
        }
	}
}

[System.Serializable]
public class AnimationSpriteCollection
{
    /// <summary>
    /// Animation Name
    /// </summary>
    public string name;
    /// <summary>
    /// Animation FPS
    /// </summary>
    public float framePerSecond = 5;
    /// <summary>
    /// Current Frame index that being rendered on SpriteRendere
    /// </summary>
    public int currentFrame = 0;
    /// <summary>
    /// Default frame index to repeat, if the animation was looping or repeating
    /// </summary>
    public int repeatToFrame = 0;
    /// <summary>
    /// How many times the animation will repeat.
    /// </summary>
    public int repeatCount = 0;
    /// <summary>
    /// Is the animation playing or not
    /// </summary>
    public bool isPlaying;
    /// <summary>
    /// Play the naimation after it finish
    /// </summary>
    public bool isLoop;
    /// <summary>
    /// Play animation forward and backward
    /// </summary>
    public bool isPingPong;
    /// <summary>
    /// List contains frame that used in animation
    /// </summary>
    public List<Sprite> frame;
}
