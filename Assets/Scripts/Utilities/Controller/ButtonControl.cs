﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(AudioSource))]
public class ButtonControl : MonoBehaviour {

    /// <summary>
    /// Signal for button command
    /// </summary>
    public Signal pressEvent;
    /// <summary>
    /// Sound when the button being clicked
    /// </summary>
    public AudioClip clickSound;
    /// <summary>
    /// Sound when mouse is over button 
    /// </summary>
    public AudioClip hoverSound;
    /// <summary>
    /// Idle button sprite
    /// </summary>
    public Sprite idle;
    /// <summary>
    /// If mouse over button sprite
    /// </summary>
    public Sprite over;
    /// <summary>
    /// If mouse press the button sprite
    /// </summary>
    public Sprite press;
    /// <summary>
    /// Button Icon
    /// </summary>
    public Sprite buttonIcon;
    /// <summary>
    /// Button Text
    /// </summary>
    public string buttonText;
    /// <summary>
    /// Button Text Font Size
    /// </summary>
    public int buttonTextFontSize;
    /// <summary>
    /// Button Text Font
    /// </summary>
    public Font buttonTextFont;
    /// <summary>
    /// Button Text Font Material
    /// </summary>
    public Material buttonTextFontMaterial;

    SpriteRenderer spriteRenderer;
    BoxCollider2D collider;
    
    void OnMouseDown() 
    {
        if (press != null)
            spriteRenderer.sprite = press;

        if (clickSound != null)
        {
            audio.clip = clickSound;
            audio.Play();
        }            

        pressEvent.Invoke();
    }

    void OnMouseEnter()
    {
        if (over != null)
            spriteRenderer.sprite = over;

        if (hoverSound != null)
        {
            audio.clip = hoverSound;
            audio.Play();
        }

    }

    void OnMouseExit()
    {
        if (idle != null)
            spriteRenderer.sprite = idle;
    }

    void OnMouseUp()
    {
        if (idle != null)
            spriteRenderer.sprite = idle;
    }

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = idle;
        collider = gameObject.AddComponent<BoxCollider2D>();
    }

    [ContextMenu("Set Button Icon")]
    public void SetButtonIcon()
    {
        GameObject clone = DeleteAndClone();
        clone.transform.localPosition = new Vector3(0, 0, 0);
        clone.transform.localScale = new Vector3(1, 1, 0);
        clone.name = "Icon";
        clone.AddComponent<SpriteRenderer>();
        clone.GetComponent<SpriteRenderer>().sprite = buttonIcon;
    }

    [ContextMenu("Set Button Text")]
    public void SetButtonText()
    {
        GameObject clone = DeleteAndClone();
        clone.transform.localPosition = new Vector3(0, 0.04f, -1);
        clone.name = "Text";
        clone.AddComponent<TextMesh>();
        TextMesh text = clone.GetComponent<TextMesh>();
        text.text = buttonText;
        text.characterSize = 0.05f;
        text.anchor = TextAnchor.MiddleCenter;
        text.alignment = TextAlignment.Center;
        text.fontSize = buttonTextFontSize * 10;
        text.font = buttonTextFont;
        clone.GetComponent<MeshRenderer>().material = buttonTextFontMaterial;
    }

    GameObject DeleteAndClone()
    {
        if (transform.childCount != 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform t = transform.GetChild(i);
                DestroyImmediate(t.gameObject);
                i--;
            }
        }

        GameObject clone = new GameObject();
        clone.transform.parent = transform;

        return clone;
    }
}
