﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Monopoly
{
    public class MonopolyManager : MonoBehaviour
    {
        public GameObject player;
        public GameObject[] dices;

        TileManager tileManager;
        int moveSteps;
        int rollTimes;
        bool isDiceRoll;
        bool isDiceAnimate;
        GameState state;

        private Vector3 lerpStart;
        private Vector3 lerpStop;
        private float lerpTime;

        GameObject titleBackground;
        GameObject title;
        GameObject playTitleButt;
        GameObject diceButt;
        GameObject homeButt;
        GameObject playButt;
        GameObject aboutButt;
        GameObject shopButt;
        GameObject aboutText;

        public enum GameState
        {
            Play,
            Pause,
            Stop,
            PlayerTurn
        }

        void Start()
        {
            Init();
        }

        void Update()
        {
            print("Move Steps : " + moveSteps + " Roll Times : " + rollTimes);
        }

        void Init()
        {
            tileManager = GameObject.Find("GameScripts").GetComponent<TileManager>();
            player = GameObject.FindGameObjectWithTag("Player");
            titleBackground = GameObject.Find("TitleBackground");
            title = GameObject.Find("Title");
            playTitleButt = GameObject.Find("PlayButtonTitle"); 
            diceButt = GameObject.Find("RollDiceButton");
            homeButt = GameObject.Find("HomeButton");
            playButt = GameObject.Find("PlayButton");
            shopButt = GameObject.Find("ShopButton");
            aboutButt = GameObject.Find("AboutButton");
            aboutText = GameObject.Find("AboutText");

            moveSteps = 0;
            rollTimes = 0;
            isDiceRoll = false;
            isDiceAnimate = false;

            SetPlayerInitPosition();

            DOTween.Init(true, true, LogBehaviour.Verbose).SetCapacity(200, 10);
        }

        void RollDice()
        {
            int dice1 = Random.Range(1, 7);
            int dice2 = Random.Range(1, 7);

            dices[0].GetComponent<SpriteControl>().isPlaying = false;
            dices[1].GetComponent<SpriteControl>().isPlaying = false;

            dices[0].GetComponent<SpriteControl>().SetFrame(dice1 - 1);
            dices[1].GetComponent<SpriteControl>().SetFrame(dice2 - 1);

            if (dice1 == dice2)
                isDiceRoll = true;
            else
                isDiceRoll = false;

            moveSteps = moveSteps + dice1 + dice2;

            if(rollTimes < 3 && isDiceRoll)
                rollTimes++;
            else
            {
                rollTimes = 0;
            }
            print("Dice 1 : " + dice1 + " Dice 2 : " + dice2);
            print("Move Steps : " + moveSteps + " Roll Times : " + rollTimes);
        }

        [Signal]
        void ClickDebug()
        {
            Debug.Log("Clicked");
        }

        [Signal]
        void RollDiceClick()
        {
            dices[0].GetComponent<SpriteControl>().isPlaying = true;
            dices[1].GetComponent<SpriteControl>().isPlaying = true;

            //Invoke("RollDice", 0.5f);
            moveSteps = 4;
            StartCoroutine(MovePlayers(moveSteps));
            state = GameState.PlayerTurn;
        }

        [Signal]
        void StartGame()
        {
            titleBackground.transform.DOMove(new Vector3(-30f, 0f, -1f), 1f);
            diceButt.transform.position = new Vector3(4.3f, 0f, 0f);
            homeButt.transform.position = new Vector3(4.3f, -1.6f, 0f);
            diceButt.GetComponent<TweenControl>().StartTween();
            homeButt.GetComponent<TweenControl>().StartTween();
            playButt.transform.DOMoveY(-10, 1).SetEase(Ease.OutBack);
            shopButt.transform.DOMoveY(-10, 1).SetEase(Ease.OutBack);
            aboutButt.transform.DOMoveY(-10, 1).SetEase(Ease.OutBack);
            title.transform.DOMove(new Vector3(4.3f, 2.2f, -2f), 1f).SetEase(Ease.OutBack);
            title.transform.DOScale(new Vector3(0.6f, 0.6f, 0f), 0.5f).SetEase(Ease.OutBack);
        }

        [Signal]
        void BackToMenu()
        {
            titleBackground.transform.DOMove(new Vector3(-3.8f, 0f, -1f), 0.5f);
            playButt.transform.DOMove(new Vector3(4.3f, 2f, 0f), 1f).SetEase(Ease.OutBack);
            shopButt.transform.DOMove(new Vector3(4.3f, 0.2f, 0f), 1f).SetEase(Ease.OutBack);
            aboutButt.transform.DOMove(new Vector3(4.3f, -1.63f, 0f), 1f).SetEase(Ease.OutBack);
            diceButt.transform.DOMoveX(8f, 1f).SetEase(Ease.OutBack);
            homeButt.transform.DOMoveX(8f, 1f).SetEase(Ease.OutBack);
            aboutText.transform.DOMoveY(8f, 1f).SetEase(Ease.OutBack);
            title.transform.DOMove(new Vector3(-2f, 0f, -2f), 1f).SetEase(Ease.OutBack);
            title.transform.DOScale(new Vector3(1.5f, 1.5f, 0f), 0.5f).SetEase(Ease.OutBack);
        }

        [Signal]
        void ShowAbout()
        {
            aboutText.transform.position = new Vector3(4.3f, -0.2f, 0f);
            homeButt.transform.position = new Vector3(4.3f, -2.46f, 0f);
            aboutText.GetComponent<TweenControl>().StartTween();
            homeButt.GetComponent<TweenControl>().StartTween();
            playButt.transform.DOMoveY(-10, 1).SetEase(Ease.OutBack);
            shopButt.transform.DOMoveY(-10, 1).SetEase(Ease.OutBack);
            aboutButt.transform.DOMoveY(-10, 1).SetEase(Ease.OutBack);
            title.transform.DOMove(new Vector3(4.3f, 2.2f, -2f), 1f).SetEase(Ease.OutBack);
            title.transform.DOScale(new Vector3(0.6f, 0.6f, 0f), 0.5f).SetEase(Ease.OutBack);
        }

        [Signal]
        void LoadMainScene()
        {
            titleBackground.transform.DOMove(new Vector3(-3.8f, 0f, -1f), 1f);
            title.transform.DOMove(new Vector3(-2f, 0f, -2f), 1f).SetEase(Ease.OutBack);
            title.transform.DOScale(new Vector3(1.5f, 1.5f, 0f), 0.5f).SetEase(Ease.OutBack);
            playTitleButt.transform.DOMove(new Vector3(-10f, 0f, 0f), 1f).SetEase(Ease.OutBack);
        }

        [Signal]
        void LoadShopScene()
        {
            Application.LoadLevel("ShopScene");
        }

        void SetPlayerInitPosition()
        {
            player.transform.position = tileManager.tile[0].GetPlayerPosition();
        }

        IEnumerator MovePlayers(int move)
        {
            while(move != 0)
            {
                int playerOrigin = tileManager.currentPlayerPosition;
                if (playerOrigin == 19)
                    player.transform.DOMove(tileManager.tile[0].GetPlayerPosition(), 1f);
                else
                    player.transform.DOMove(tileManager.tile[playerOrigin + 1].GetPlayerPosition(), 1f);
                yield return new WaitForSeconds(1.2f);
                move--;
                moveSteps = move;
            }            
        }

        IEnumerator StupidPlayerMover(int move)
        {
            int playerOrigin = tileManager.currentPlayerPosition;
            if (move >= 3)
            {
                Debug.Log("ha");
                player.transform.DOMove(tileManager.tile[playerOrigin + 1].GetPlayerPosition(), 1f);
                yield return new WaitForSeconds(1.2f);
            }
            if (move >= 2)
            {
                Debug.Log("haha");
                player.transform.DOMove(tileManager.tile[playerOrigin + 1].GetPlayerPosition(), 1f);
                yield return new WaitForSeconds(1.2f);
            }
            if (move >= 1)
            {
                Debug.Log("hahe");
                player.transform.DOMove(tileManager.tile[playerOrigin + 1].GetPlayerPosition(), 1f);
                yield return new WaitForSeconds(1.2f);
            }
            moveSteps = move;    
        }

        void PlayerLerper(Vector3 startPos, Vector3 endPos, float startTime)
        {
            float distance = Vector3.Distance(startPos, endPos);
            float distCovered = (Time.time - startTime) * 0.1f;
            float fracJourney = distCovered / distance;
            transform.position = Vector3.Lerp(startPos, endPos, fracJourney);

        }

    }
}