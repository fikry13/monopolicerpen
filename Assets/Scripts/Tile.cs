﻿using UnityEngine;
using System.Collections;

namespace Monopoly
{
    [System.Serializable]
    public class Tile
    {
        /// <summary>
        /// Tell the status of the tile.
        /// </summary>
        public enum TileStatus
        {
            Unavailable,
            Available,
            Sold
        }

        /// <summary>
        /// Tile Name.
        /// </summary>
        public string tileName;
        /// <summary>
        /// Tile Price.
        /// </summary>
        public float tilePrice;
        /// <summary>
        /// Set or Get the First Point of the tile, Usually placed at bottom-left.
        /// </summary>
        public Vector2 first;
        /// <summary>
        /// Set or Get the Second Point of the tile, Usually placed at top-right.
        /// </summary>
        public Vector2 second;
        /// <summary>
        /// Set or Get the tile Status.
        /// </summary>
        public TileStatus tileStatus;
        /// <summary>
        /// Check if the player is in the tile.
        /// </summary>
        public bool isPlayerHere;
        
        Vector2 playerPosition;
        
        /// <summary>
        /// This method is used to get the appropriate position of the player in the tile.
        /// </summary>
        /// <returns>And it will return the position, where the game can put the player in the tile.</returns>
        public Vector2 GetPlayerPosition()
        {
            Vector2? satu = first;
            Vector2? dua = second;

            if (satu.HasValue && dua.HasValue)
            {
                playerPosition = new Vector3(satu.Value.x + ((dua.Value.x - satu.Value.x) / 2), satu.Value.y + ((dua.Value.y - satu.Value.y) / 2));
            }
            return playerPosition;
        }

    }
}
